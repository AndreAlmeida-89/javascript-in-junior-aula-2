let alunos = [
    {
        "nome": "Pedro",
        "turma": "A",
        "nota1": 10,
        "nota2": 7
    },
    {
        "nome": "André",
        "turma": "A",
        "nota1": 10,
        "nota2": 8
    },
    {
        "nome": "Maria",
        "turma": "B",
        "nota1": 7,
        "nota2": 4
    },
    {
        "nome": "Claudinei",
        "turma": "B",
        "nota1": 9,
        "nota2": 5
    },
    {
        "nome": "Ivi",
        "turma": "C",
        "nota1": 10,
        "nota2": 10
    },
    {
        "nome": "Jonathan",
        "turma": "C",
        "nota1": 9,
        "nota2": 9
    }
]
function melhoresAlunos (alunos) {
    //Cria array com as turmas e adiciona a propiedade 'média' ao objeto. 
    let arrTurmas = [];
    for (let j in alunos){
        arrTurmas.push(alunos[j].turma);
        alunos[j].media = (alunos[j].nota1 + alunos[j].nota2) / 2.0;
    }

     //Exclui as turmas duplicadas.
     arrTurmas = [... new Set(arrTurmas)];

    //Itera a array das turmas.
    for (let i in arrTurmas){
        //Declara 'melhorAluno' da turma e atribui 0 a sua média.
        let melhorAluno = "";
        let melhorMedia = 0;
        //Itera a array de objetos 'alunos' para cada turma.
        for (let k in alunos){
            //Verifica se o k-ésimo aluno pertence à i-ésima turma e se sua média é maior que a do melhor aluno da turma.
            if (alunos[k].turma === arrTurmas[i] && alunos[k].media > melhorMedia){
                //Atribui seu nome e media ao melhor aluno e melhor media, respectivamente.
                melhorAluno = alunos[k].nome;
                melhorMedia = alunos[k].media;
            }
        }
        //Gera string e a imprime no console.
        let texto = "O aluno " + melhorAluno + " teve a media mais alta da turma " + arrTurmas[i] + ", com " + melhorMedia;
        console.log(texto);
    }
    
}


melhoresAlunos(alunos);